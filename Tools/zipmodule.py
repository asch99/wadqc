#!/usr/bin/env python
from __future__ import print_function
"""
Changelog:
  20160909: output to current folder, not parent
  20160513: python3 compatible
  20160418: based on pywad2zip.py version 20150831
"""

__version__ = '20160909'
__author__ = 'aschilham'

import argparse
import sys
import os.path as path
import os
import zipfile

# First rule: exclude these directories completely
excludedirs = [
    '.git',
    'Tools',
    'Testing',
    'Documentation',
    '1', 'wpr', # some personal rubbish
]

# Second rule: exclude all filenames that match these exactly (in any folder)
excludefiles = [
    'error.log'
]

# Third rule: only include filenames that match these extensions (in any folder)
includeextensions = [
    '.py','.exe'
]

#------------
## main
if __name__=="__main__":
    parser = argparse.ArgumentParser(description=
            'WAD-Python module zipper version %s.' 
            'Use it to generate zips for uploading to the WAD server.'
            'E.g. if the executable is path/mymodule.py, invoke' 
            '  zipmodule.py -e dapath/mymodule.py ' 
            ' to create mymodule.py.zip containing all .py and .exe files in dapath'%__version__)

    filename = None
    mode = 'list'
    parser.add_argument('-e','--exe',
                        default=filename,type=str,
                        help='path to the executable',dest='filename')

    parser.add_argument('-m','--mode',
                        default=mode,type=str,
                        help='run mode (list or zip)',dest='mode')

    modes = ['list','zip']

    args = parser.parse_args()
    if args.mode not in modes:
        print('ERROR! "%s" is not a valid mode!\n\n'%args.mode)
        parser.print_help()
        sys.exit()

    if args.filename is None or not args.filename.lower().endswith('.py'):
        print('ERROR! "%s" is not a python file!\n\n'%args.filename)
        parser.print_help()
        sys.exit()

    # make sure the working dir is the folder containing the indicated executable
    rootdir = os.path.abspath(os.path.dirname(os.path.abspath(args.filename)))

    outdir = os.path.abspath(os.path.dirname(os.path.abspath(__file__)))
    # create zipfile
    if args.mode == 'zip':
        outfile = os.path.join(outdir,os.path.basename(args.filename)+'.zip')
        zf = zipfile.ZipFile(outfile, 'w',zipfile.ZIP_DEFLATED)

    print('zip will contain')
    # find all files that match the given criteria
    count = 0
    for subdir, dirs, files in os.walk(rootdir,topdown=True):
        dirs[:] = [d for d in dirs if d not in excludedirs]
        files[:] = [f for f in files if f not in excludefiles]
        for fname in files:
            for ext in includeextensions:
                if fname.endswith(ext):
                    filename = os.path.join(subdir, fname)
                    daf = os.path.relpath(filename, start=rootdir)
                    if args.mode == 'zip':
                        zf.write(filename,daf)
                    print('  ',daf)
                    count += 1

    # close zipfile
    if args.mode == 'zip':
        print("Done. Packed %d files in %s"%(count,os.path.join(outdir,outfile)))
        zf.close()
    else:
        print("%d files meet the criteria. Just listing, no zip file created."%count)

