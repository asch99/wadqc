# Import Form 
from flask_wtf import FlaskForm
from wtforms import HiddenField, FormField

class SendForm(FlaskForm):
    posttable = HiddenField()

class DownloadForm(FlaskForm):
    posttable = HiddenField()

class EditorForm(FlaskForm):
    sendform = FormField(SendForm)
    downloadform = FormField(DownloadForm)
