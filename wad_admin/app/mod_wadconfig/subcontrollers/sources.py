from flask import Blueprint, render_template, Markup, url_for, redirect, request, flash
import os
try:
    from app.mod_auth.controllers import login_required
    from app.libs import html_elements
    from app.libs.shared import dbio_connect
except ImportError:
    from wad_admin.app.mod_auth.controllers import login_required
    from wad_admin.app.libs import html_elements
    from wad_admin.app.libs.shared import dbio_connect
dbio = dbio_connect()

# logging
from werkzeug.local import LocalProxy
from flask import current_app
logger = LocalProxy(lambda: current_app.logger)

# Import modify forms
from .forms_sources import ModifyForm
from .forms_confirm import ConfirmForm

mod_blueprint = Blueprint('wadconfig_sources', __name__, url_prefix='/wadadmin')

@mod_blueprint.route('/sources/')
@login_required
def default():
    # display and allow editing of modules table
    subtitle='Deleting a data source sets off a cascade: It also deletes all entries '\
           'that directly reference that data source, and all other entries that reference those entries. '\
           'That includes: processes, results. '\
           'The number of references is shown in #references.'

    stuff = dbio.DBDataSources.select()

    table_rows = []
    for data in stuff:
        table_rows.append([data.id, html_elements.Link(label=data.name, href=url_for('.inspect', gid=data.id)), data.source_type.name,
                           len(data.processes)+len(data.results),
                           html_elements.Button(label='delete', href=url_for('.delete', gid=data.id), _class='btn btn-danger'),
                           html_elements.Button(label='edit', href=url_for('.modify', gid=data.id))
                           ])

    table = html_elements.Table(headers=['id', 'name', 'source_type', '#references'], 
                                rows=table_rows,
                                _class='tablesorter-wadred', _id='sortTable')
    newbutton = html_elements.Button(label='New', href=url_for('.modify'))
    page = table+newbutton
    
    return render_template("wadconfig/generic.html", title='Data Sources', subtitle='', msg='', html=Markup(page),
                           inpanel={'type': "panel-warning", 'title': "WARNING", 'content':subtitle})

@mod_blueprint.route('/sources/delete/', methods=['GET', 'POST'])
@login_required
def delete():
    """
    delete given id of given table from iqc db
    """
    _gid = int(request.args['gid']) if 'gid' in request.args else None

    # invalid table request are to be ignored
    if _gid is None:
        logger.error("Delete source called with no valid source id")
        return redirect(url_for('.default'))
    
    try:
        src = dbio.DBDataSources.get_by_id(_gid)
        if not src:
            logger.error("Delete source called with no valid source id")
            return redirect(url_for('.default'))
    except dbio.DBDataSources.DoesNotExist:
        logger.error("Delete source called with no valid source id")
        return redirect(url_for('.default'))


    # ask for confirmation if coupled to results or processes
    if (len(src.processes)+len(src.results))>0:
        # invalid table request are to be ignored
        formtitle = 'Confirm action: delete source with {} coupled results and {} coupled processes'.format(len(src.results), len(src.processes))
        msg = [
            'This will also delete those results and processes.',
            'Tick confirm and click Submit to proceed.'
        ]
        form = ConfirmForm(None if request.method=="GET" else request.form)
    
        # Verify the sign in form
        valid = True
        if form.validate_on_submit():
            # check if this is a new module
            if form.confirm.data is False:
                flash('Must tick confirm!', 'error')
                valid = False
    
            if valid:
                # do stuff
                src.delete_instance(recursive=True)
                # go back to overview page
                return redirect(url_for('.default'))
    
        return render_template("wadconfig/confirm.html", form=form, 
                               action=url_for('.delete', gid=_gid),
                               title=formtitle, msg=msg)

    else:
        src.delete_instance(recursive=True)
        # go back to overview page
        return redirect(url_for('.default'))


@mod_blueprint.route('/sources/modify/', methods=['GET', 'POST'])
@login_required
def modify():
    _gid = int(request.args['gid']) if 'gid' in request.args else None

    # invalid table request are to be ignored
    # for now only orthanc is supported
    formtitle = 'Modify datasource'
    form = ModifyForm(None if request.method=="GET" else request.form)
    if not _gid is None:
        src = dbio.DBDataSources.get_by_id(_gid)
        form.name.data = src.name
        form.currentname.data = src.name
        form.source_type.data = src.source_type #dropdown
        form.protocol.data = src.protocol # dropdown
        form.aetitle.data = src.aetitle
        form.host.data = src.host
        form.port.data = src.port
        form.user.data = src.user
        form.pswd.data = src.pswd
        form.gid.data = _gid
    if form.gid is None or form.gid.data == '' or form.gid.data is None: #define here, to avoid wrong label on redisplaying a form with errors
        formtitle = 'New data source'
    existingnames = [v.name for v in dbio.DBDataSources.select()]

    # Verify the sign in form
    valid = True
    if form.validate_on_submit():
        # check if this is a new module
        if form.currentname.data is None or form.currentname.data == 'None': # yes, new form
            if form.name.data in existingnames:
                flash('A data source with this name already exist.', 'error')
                valid = False
        else: # not a new form
            if existingnames.count(form.name.data)> (0 if not form.currentname.data == form.name.data else 1) :
                flash('A data source with this name already exist.', 'error')
                valid = False
        if valid:
            # the elements of the form
            field_dict = {k:v for k,v in request.form.items()}
            igid = 0
            if 'gid' in field_dict:
                try:
                    igid = int(field_dict['gid'])
                except:
                    pass
                
            if igid>0: # update, not create
                src = dbio.DBDataSources.get_by_id(igid)
                src.name = field_dict['name']
                src.source_type = field_dict['source_type']
                src.protocol = field_dict['protocol']
                src.aetitle = field_dict['aetitle']
                src.host = field_dict['host']
                src.port = field_dict['port']
                src.user = field_dict['user']
                if len(form.pswd.data)>0:
                    src.pswd = field_dict['pswd']
                src.save()
            else:
                dbio.DBDataSources.create(**field_dict)
            return redirect(url_for('.default'))
            
    msg = [
        'Use this form to update the access method WAD-QC uses for an existing Source. ',
        'The Source itself will not be modified; check the documentation of the used Source if you want to modify the Source itself.',
        'Fill out the fields and click Submit'
    ]
    return render_template("wadconfig/sources_modify.html", form=form, action='.', #action=url_for('.upload_file'),
                           title=formtitle, msg=msg)
        
@mod_blueprint.route('/sources/inspect', methods=['GET', 'POST'])
@login_required
def inspect():
    # display data sets in sources
    _gid   = int(request.args['gid']) if 'gid' in request.args else None
    # invalid table request are to be ignored
    if _gid is None:
        # go back to overview page
        return redirect(url_for('.default'))

    _level  = request.args.get('level', 'patients')
    _patuid = request.args.get('patuid', None)
    _stuuid = request.args.get('stuuid', None)
    _seruid = request.args.get('seruid', None)
        
    # make a PACS connection
    from wad_qc.connection.pacsio import PACSIO
    pacsconfig = dbio.DBDataSources.get_by_id(_gid).as_dict()
    
    # 1. make sure a local pacs is running and that it has the required demo data
    try:
        pacsio = PACSIO(pacsconfig)
    except Exception as e:
        msg = ' '.join(['Cannot access PACS %s with provided credentials'%pacsconfig['name'],str(e)])
        logger.error(msg)
        return render_template("wadconfig/generic.html", title='PACS Access', subtitle='', msg='',
                               inpanel={'type': "panel-danger", 'title': "ERROR", 'content':msg})

    # make a dropdown selector
    idname = [ [tn, tn] for tn in ['patients', 'studies', 'series', 'instances'] ]
    plink = "?gid={}".format(_gid)
    # using dropdown will now drop _patuid, _stuuid, _seruid
    #if _patuid: plink += "&patuid={}".format(_patuid)
    #if _stuuid: plink += "&stuuid={}".format(_stuuid)
    #if _seruid: plink += "&seruid={}".format(_seruid)
    picker = html_elements.Picker(name="pacstable", idname=idname, sid=_level,
                                  fun=Markup('location.href = "{}&level="+jQuery(this).val();'.format(plink)))

    # make table
    table_rows = []
    try:
        if _level == 'patients':
            """
            # in future want something like this:
            ids = pacsio.getPatientIds()
            for i in ids:
                stuff.append(pacsio.getPatient(i))
            display = ['ID', 'MainDicomTags/PatientName', 'MainDicomTags/PatientID', 'Studies'] 
            """
            display = ['PatientUID'] 
            ids = pacsio.getPatientIds()
            for i in ids:
                table_rows.append( [
                    html_elements.Link(label=i, href=url_for('.inspect', gid=_gid, level='studies', patuid=i)),
                    ] )

        elif _level == 'studies':
            display = ['PatientUID', 'StudyUID']
            ids = pacsio.getStudyIds(patientid=_patuid if not _patuid == '' else None)
            for i in ids:
                table_rows.append( [
                    '' if _patuid is None else html_elements.Link(label=_patuid, href=url_for('.inspect', gid=_gid, level='patients')),
                    html_elements.Link(label=i, href=url_for('.inspect', gid=_gid, level='series', patuid=_patuid, stuuid=i)),
                    ] )

        elif _level == 'series':
            display = ['StudyUID', 'SeriesUID']
            ids = pacsio.getSeriesIds(studyid=_stuuid if not _stuuid == '' else None)
            for i in ids:
                table_rows.append( [
                    '' if _stuuid is None else html_elements.Link(label=_stuuid, href=url_for('.inspect', gid=_gid,level='studies', patuid=_patuid, stuuid=_stuuid, seruid=i)),
                    html_elements.Link(label=i, href=url_for('.inspect', gid=_gid,level='instances', patuid=_patuid, stuuid=_stuuid, seruid=i)),
                    ] )

        elif _level == 'instances':
            display = ['SeriesUID', 'instanceUID']
            ids = pacsio.getInstancesIds(seriesid=_seruid if not _seruid == '' else None)
            for i in ids:
                table_rows.append( [
                    '' if _seruid is None else html_elements.Link(label=_seruid, href=url_for('.inspect', gid=_gid,level='series', patuid=_patuid, stuuid=_stuuid, seruid=_seruid)),
                    i] )

    except Exception as e:
        logger.info(' '.join(['Cannot access at level %s; error'%(_level),str(e)]))
    

    table = html_elements.Table(headers=display, rows=table_rows,
                                _class='tablesorter-wadred', _id='sortTable')
    page = picker+table
    
    return render_template("wadconfig/generic.html", title='PACS Access', 
                           subtitle='entries: %d'%len(table_rows), msg='', html=Markup(page))

