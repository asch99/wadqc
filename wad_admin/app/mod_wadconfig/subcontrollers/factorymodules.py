from flask import Blueprint, render_template, Markup, url_for, redirect, request, flash
import os
import tempfile
from io import BytesIO
try:
    from app.mod_auth.controllers import login_required
    from app.libs import html_elements
    from app.libs.shared import dbio_connect, upload_file
    from app.libs.exchange import export_configs, import_configs
    from app.libs.modulerepos import check_repo, is_release_candidate, is_newer_release, install_from_url, replace_from_url
except ImportError:
    from wad_admin.app.mod_auth.controllers import login_required
    from wad_admin.app.libs import html_elements
    from wad_admin.app.libs.shared import dbio_connect, upload_file
    from wad_admin.app.libs.exchange import export_configs, import_configs
    from wad_admin.app.libs.modulerepos import check_repo, is_release_candidate, is_newer_release, install_from_url, replace_from_url

dbio = dbio_connect()

# logging
from werkzeug.local import LocalProxy
from flask import current_app
logger = LocalProxy(lambda: current_app.logger)

from .forms_confirm import ConfirmForm

mod_blueprint = Blueprint('wadconfig_factorymodules', __name__, url_prefix='/wadadmin')

@mod_blueprint.route('/factorymodules/')
@login_required
def default():
    """
    Query GitHub for latest modules. Offer upgrade if installed, else offer installation.
    """
    all_releases = request.args.get('all', None) == "1"

    subtitle = "These Modules are available in the online repository. Replacing or updating a module will "\
        "also update all ModuleConfigs that are not coupled to a Result, Process, or Selector. "\
        "Manually update the ModuleConfig of a Selector if required. Also, to rerun a Process or Result "\
        "with an updated ModuleConfig, delete it and resend it through DBTool."

    available_modules, msg = check_repo(dbio, logger=logger)

    # check if want to show all releases
    has_multiple_releases = False
    for key,vals in available_modules.items():
        if len(vals)>1:
            has_multiple_releases = True
            break
        if is_release_candidate(vals[0]['release_version']):
            has_multiple_releases = True
            break
        
    if has_multiple_releases: 
        btnLabel =  'Show all releases' if not all_releases else 'Show only latest releases'
        btnFlags = {}
        if not all_releases: btnFlags['all'] = 1
        btnAllReleases = html_elements.Button(label=btnLabel, href=url_for('.default', **btnFlags ))


    if not all_releases:
        # keep only latest release for each module and ignore release candidates
        updated_mods = {}
        for key,mods in available_modules.items():
            latest_version = None
            for mod in mods:
                nwversion = mod['release_version']
                try:
                    if not is_release_candidate(nwversion):
                        if latest_version is None:
                            latest_version = nwversion
                            updated_mods[key] = [mod]
                        elif is_newer_release(latest_version, nwversion):
                            latest_version = mod['release_version']
                            updated_mods[key] = [mod]
                except Exception as e:
                    logger.error('Module "{}" has an invalid version number "{}". Must be like "v1.3" or "v1.3-rc.2"'.format(key, nwversion))
        available_modules = updated_mods

    installed_modules = [ m.name for m in dbio.DBModules.select() ]
    table_rows = []
    for key in sorted(list(available_modules.keys())):
        for avail in available_modules[key]:
            installed = False
            installed_version = 'none'
            note = ''
            if key in installed_modules:
                m = dbio.DBModules.get_by_name(key)
                installed = True
                installed_version = m.repo_version
                installed_url     = m.repo_url
                
                if not avail['repo_url'] == installed_url:
                    note = 'repository url differs!'
                elif is_newer_release(installed_version, avail['release_version']):
                    note = 'update available'
                elif avail['release_version'] == installed_version:
                    note = 'installed version'
    
            valid = True
            try:
                nwversion = avail['release_version']
                dummy = is_release_candidate(nwversion)
            except Exception as e:
                note = "Invalid version number"
                logger.error('Module "{}" has an invalid version number "{}". Must be like "v1.3" or "v1.3-rc.2"'.format(key, nwversion))
                valid = False

            if valid:
                if not installed:
                    btn = html_elements.Button(label='install', href=url_for('.install', release_url=avail['release_url']), _class='btn btn-primary')
                elif is_newer_release(installed_version, avail['release_version']):
                    btn = html_elements.Button(label='update', href=url_for('.replace', mod_id=m.id, release_url=avail['release_url']), _class='btn btn-warning')
                elif avail['release_version'] == installed_version:
                    btn = html_elements.Button(label='reinstall', href=url_for('.replace', mod_id=m.id, release_url=avail['release_url']), _class='btn btn-warning')
                else:
                    btn = html_elements.Button(label='replace', href=url_for('.replace', mod_id=m.id, release_url=avail['release_url']), _class='btn btn-danger')
            else:
                btn = ""

            table_rows.append([key, avail['release_version'], avail['release_date'], avail['release_note'],
                               html_elements.Link(label='documentation', href=avail['repo_dox']),
                               installed_version,
                               note,
                               btn,
                               ])

    table = html_elements.Table(headers=['name', 'repo_version', 'repo_date', 'repo_note', 'repo_dox', 
                                      'installed_version', 'NOTE' ], rows=table_rows,
                             _class='tablesorter-wadred', _id='sortTable')
    if has_multiple_releases:
        page = btnAllReleases + table
    else:
        page = table
    
    return render_template("wadconfig/generic.html", title='Modules in Repository', subtitle='', msg=msg, html=Markup(page),
                           inpanel={'type': "panel-info", 'title': "info", 'content':subtitle})


@mod_blueprint.route('/factorymodules/install', methods=['GET', 'POST'])
@login_required
def install():
    """
    Download and install zipball
    """
    _release_url = request.args.get('release_url', None)
    if _release_url is None:
        return redirect(url_for('.default'))
    
    repo_url, repo_version = _release_url.split('/zipball/')

    # invalid table request are to be ignored
    formtitle = 'Confirm action: install module version {} from {}'.format(repo_version, repo_url)
    msg = [
        'This will download and install the Module and ModuleConfigs.',
        '',
        'Tick confirm and click Submit to proceed.'
    ]
    form = ConfirmForm(None if request.method=="GET" else request.form)

    # Verify the sign in form
    valid = True
    if form.validate_on_submit():
        # check if this is a new module
        if form.confirm.data is False:
            flash('Must tick confirm!', 'error')
            valid = False

        if valid:
            # do stuff
            return _do_install(_release_url, {'repo_url': repo_url, 'repo_version': repo_version})

    return render_template("wadconfig/confirm.html", form=form, 
                           action=url_for('.install', release_url=_release_url),
                           title=formtitle, msg=msg)

@mod_blueprint.route('/factorymodules/replace', methods=['GET', 'POST'])
@login_required
def replace():
    """
    Download and install zipball
    """
    _release_url = request.args.get('release_url', None)
    _mod_id = request.args.get('mod_id', None)
    if _release_url is None or _mod_id is None:
        return redirect(url_for('.default'))
    
    repo_url, repo_version = _release_url.split('/zipball/')
    
    # invalid table request are to be ignored
    mod_name = dbio.DBModules.get_by_id(_mod_id).name
    formtitle = 'Confirm action: replace module "{}" with module version {} from {}'.format(mod_name, repo_version, repo_url)
    msg = [
        'This will download the Module and ModuleConfigs. The old Module will be overwritten, '
        'the old ModuleConfigs will be deleted and new ModuleConfigs will be installed. ',
        'This will not automatically update the ModuleConfigs in use by Selectors, Processes or Results.',
        '',
        'Tick confirm and click Submit to proceed.'
    ]
    form = ConfirmForm(None if request.method=="GET" else request.form)

    # Verify the sign in form
    valid = True
    if form.validate_on_submit():
        # check if this is a new module
        if form.confirm.data is False:
            flash('Must tick confirm!', 'error')
            valid = False

        if valid:
            # do stuff
            return _do_replace(_release_url, {'repo_url': repo_url, 'repo_version': repo_version}, _mod_id)

    return render_template("wadconfig/confirm.html", form=form, 
                           action=url_for('.replace', release_url=_release_url, mod_id=_mod_id),
                           title=formtitle, msg=msg)


def _do_install(url, repo_info):
    """
    """
    title='Install factory Module'
    error, msg = install_from_url(dbio, url, repo_info, logger)
    if error:
        return render_template("wadconfig/generic.html", title=title, subtitle='', msg='',
                               inpanel={'type': "panel-danger", 'title': "ERROR", 'content':msg})

    else:
        return render_template("wadconfig/generic.html", title=title, subtitle='', msg='', html="",
                               inpanel={'type': "panel-success", 'title': "Success", 'content':msg})

def _do_replace(url, repo_info, mod_id):
    """
    """
    title = 'Replace installed Module by factory Module'
    error, msg = replace_from_url(dbio, url, repo_info, mod_id, logger)
    if error:
        return render_template("wadconfig/generic.html", title=title, subtitle='', msg='',
                               inpanel={'type': "panel-danger", 'title': "ERROR", 'content':msg})

    else:
        return render_template("wadconfig/generic.html", title=title, subtitle='', msg='', html="",
                               inpanel={'type': "panel-success", 'title': "Success", 'content':msg})
