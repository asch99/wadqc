# Import Form 
from flask_wtf import FlaskForm 
from flask import Markup
# Import Form elements such as TextField 
from wtforms import HiddenField, BooleanField, FieldList, FormField, FileField

# Import Form validators
from wtforms.validators import Required, NoneOf, NumberRange

class ConfigEntryForm(FlaskForm):
    # prefix with cfg_ to prevent no display due to clashes.
    cfg_name = HiddenField()
    cfg_description = HiddenField()
    cfg_origin = HiddenField()
    cfg_selector = HiddenField()
    cfg_hasmeta = HiddenField()
    cid = HiddenField()
    cfg_selected = BooleanField('include in export')

class ExportForm(FlaskForm):
    gid = HiddenField('gid', [])
    configs = FieldList(FormField(ConfigEntryForm))

class ImportForm(FlaskForm):
    gid = HiddenField('gid', [])
    file = FileField('File', [])  
