#!/usr/bin/env python
from __future__ import print_function

import argparse
import sys
import os
import errno
import shutil
import subprocess
import platform
import shutil

__version__ = '20180807' 
"""
orthanc120fix_Lin64_Ubuntu1610.tar.gz: 
  ./orthanc_setup.py -m build -d postgresql -b Orthanc-1.2.0 -p fix360 
orthanc130fix_Lin64_Ubuntu1704.tar.gz: 
  ./orthanc_setup.py -m build -d postgresql -b Orthanc-1.3.0 -p fix362threads,fix164inf
orthanc130fix_Lin64_Ubuntu1710.tar.gz: 
  ./orthanc_setup.py -m build -d postgresql -b Orthanc-1.3.0 -p fix362threads,fix164inf
orthanc141fix_Lin64_Ubuntu1804.tar.gz: # OrthancPostgreSQL-2.2 seems broken (pg_trm and globalproperties)
  ./orthanc_setup.py -m build -d postgresql -b Orthanc-1.4.1 -bp OrthancPostgreSQL-2.2 -bw OrthancWebViewer-2.4 -p fix164inf,fixpluginlogging
  ./orthanc_setup.py -m build -d postgresql -b Orthanc-1.4.1 -bp OrthancPostgreSQL-2.1 -bw OrthancWebViewer-2.4 -p fix164inf,fixpluginlogging
orthanc141fix_Lin64_Ubuntu1604.tar.gz: # OrthancPostgreSQL-2.2 seems broken (pg_trm and globalproperties)
  ./orthanc_setup.py -m build -d postgresql -b Orthanc-1.4.1 -bp OrthancPostgreSQL-2.2 -bw OrthancWebViewer-2.4 -p fix164inf,fixpluginlogging
  ./orthanc_setup.py -m build -d postgresql -b Orthanc-1.4.1 -bp OrthancPostgreSQL-2.1 -bw OrthancWebViewer-2.4 -p fix164inf,fixpluginlogging

after compilation finished successfully:
cd ~/WADDEV
tar -cvzf orthanc130fix_Lin64_Ubuntu1704.tar.gz orthanc/bin orthanc/plugins
tar -cvzf orthanc141fix_Lin64_Ubuntu1804.tar.gz orthanc/bin orthanc/plugins

MySQL:
sandbox https://www.dbdeployer.com/
Orthanc: https://www.orthanc-server.com/static.php?page=mysql
http://www.fromdual.com/how-to-install-multiple-mysql-instances-on-a-single-host-using-myenv
https://dev.mysql.com/downloads/mysql/

Changelog:
 20180807: orthanc1.4.1, orthanc-posgresql2.1, webviewer-2.4, orthanc-postgresql2.2
 20171031: fix broken argument branch
 20170912: Updated to Orthanc 1.3.0; timeout fix no longer needed, but threads fix is needed; 
           add fix for PostgreSQL plugin for static boost 1.64.0 and PostgreSQL 9.6.1
 20170613: Added ORTHANCBRANCH
 20170503: added custom ports for PostgreSQL, REST, PACS
 20170314: fix for timeout in dcmtk-3.6.0 (on Philips US)
 20160529: first python version

"""
DEVROOT = os.path.expanduser('~/WADDEV')
PACS_PASS = 'waddemo'
DB_PASS = 'waddemo'
PSQL_PORT = '5432'
REST_PORT = '8042'
PACS_PORT = '11112'
ORTHANCBRANCH = 'default'
ORTHANCBRANCH_1_2_0 = 'Orthanc-1.2.0'
ORTHANCBRANCH_1_3_0 = 'Orthanc-1.3.0'
ORTHANCBRANCH = 'Orthanc-1.4.1'
POSTGRESQLBRANCH = 'default'
POSTGRESQLBRANCH_2_1 = 'OrthancPostgreSQL-2.1'
POSTGRESQLBRANCH = 'OrthancPostgreSQL-2.2'

WEBVIEWERBRANCH = 'default '
WEBVIEWERBRANCH = 'OrthancWebViewer-2.4'

def delete_old():
    # clean start
    print("...cleaning up old installation...")
    shutil.rmtree(os.path.join(DEVROOT, 'orthanc', 'sources'), ignore_errors=True)
    shutil.rmtree(os.path.join(DEVROOT, 'orthanc', 'bin'), ignore_errors=True)

def create_folders():
    # create needed directories
    print("...creating directory structure...")
    folders = [
        os.path.join(DEVROOT, 'orthanc', 'db'),
        os.path.join(DEVROOT, 'orthanc', 'lua'),
        os.path.join(DEVROOT, 'orthanc', 'config'),
        os.path.join(DEVROOT, 'orthanc', 'plugins'),
        os.path.join(DEVROOT, 'orthanc', 'bin'),
        os.path.join(DEVROOT, 'orthanc', 'sources'),
        os.path.join(DEVROOT, 'WAD_QC'),
    ]
    for folder in folders:
        try:
            os.makedirs(folder)
        except OSError as e: 
            if e.errno == errno.EEXIST and os.path.isdir(folder):
                pass
            else:
                raise

def copy_replaces(src, dest, inlist, outlist):
    # read the template from src, and write to dest, 
    #   whilst replacing each placeholder in inlist by the one in outlist
    with open(src, mode='r') as f:
        data = f.read()

    for old, new in zip (inlist, outlist):
        data = data.replace(old, new)

    with open(dest, mode='w') as f:
        f.write(data)

def create_scripts(database, branchpg, patches=[]):
    # create proper paths in build files and config
    print("...creating proper scripts from templates...")
    opsys = platform.system() # Linux, Darwin, Windows

    # build from source scripts
    inlist = ['__DEVROOT__']
    outlist = [DEVROOT]
    if opsys == 'Darwin':
        inlist.extend([
            'grep -c ^processor /proc/cpuinfo',
            'libOrthancWebViewer.so',
            'libOrthancPostgreSQLIndex.so',
            'libOrthancPostgreSQLStorage.so',
            '__MACFLAGS__'
        ])
        outlist.extend([
            'sysctl -n hw.ncpu',
            'libOrthancWebViewer.dylib',
            'libOrthancPostgreSQLIndex.dylib',
            'libOrthancPostgreSQLStorage.dylib',
            'CXXFLAGS="-D_FORTIFY_SOURCE=0" CFLAGS="-D_FORTIFY_SOURCE=0"'
        ])
    else:
        inlist.extend([
            '__MACFLAGS__'
        ])
        outlist.extend([
            ''
        ])

    if 'fix360' in patches:
        fname = 'build_fix360.sh'
        shutil.copyfile(src='timeout_fix.zip', dst=os.path.join(DEVROOT, 'orthanc', 'sources', 'timeout_fix.zip'))
    elif 'fix362threads' in patches:
        fname = 'build_fix362.sh'
        shutil.copyfile(src='threads362_fix.zip', dst=os.path.join(DEVROOT, 'orthanc', 'sources', 'threads362_fix.zip'))
    else:
        fname = 'build.sh'

    copy_replaces(src=os.path.join('templates', fname), 
                  dest=os.path.join(DEVROOT, 'orthanc', 'sources', 'build.sh'), 
                  inlist=inlist, 
                  outlist=outlist) 
    copy_replaces(src=os.path.join('templates', 'build-webviewer.sh'), 
                  dest=os.path.join(DEVROOT, 'orthanc', 'sources', 'build-webviewer.sh'), 
                  inlist=inlist, 
                  outlist=outlist) 

    # postgresql support
    if database == 'postgresql':
        fname = 'build-postgresql.sh'
        if 'fix164inf' in patches:
            pgfixflags = "-DHAVE_ISINF=ON"
        else:
            pgfixflags = "-DHAVE_ISINF"
        if 'fixpluginlogging' in patches:
            pgfixflags += " -DORTHANC_ENABLE_LOGGING_PLUGIN=ON"
        inlist.extend(['__FIXPGFLAGS__'])
        outlist.extend([pgfixflags])

        # POSTGRESQLBRANCH = 'OrthancPostgreSQL-2.2'
        bversion = [ int(b) for b in branchpg.split('-')[1].split('.')]
        if bversion[0]>2 or (bversion[0]==2 and bversion[1]>=2):
            fname = 'build-postgresql2.sh'
        else:
            fname = 'build-postgresql.sh'
        copy_replaces(src=os.path.join('templates', fname), 
                      dest=os.path.join(DEVROOT, 'orthanc', 'sources', 'build-postgresql.sh'), 
                      inlist=inlist, 
                      outlist=outlist) 

        inlist = ['__DEVROOT__', '__PACSPSWD__', '__DBPSWD__', '__PSQLPORT__', '__RESTPORT__', '__PACSPORT__']
        outlist = [DEVROOT, PACS_PASS, DB_PASS, PSQL_PORT, REST_PORT, PACS_PORT]
        copy_replaces(src=os.path.join('templates', 'orthanc_postgresql.json'), 
                      dest=os.path.join(DEVROOT, 'orthanc', 'config', 'orthanc_postgresql.json'), 
                      inlist=inlist, 
                      outlist=outlist) 

    # sqlite only
    copy_replaces(src=os.path.join('templates', 'orthanc.json'), 
                  dest=os.path.join(DEVROOT, 'orthanc', 'config', 'orthanc.json'), 
                  inlist=inlist, 
                  outlist=outlist) 

    # lua scripts
    inlist = ['__DEVROOT__']
    outlist = [DEVROOT]
    copy_replaces(src=os.path.join('templates', 'wad_onstablestudy.lua'), 
                  dest=os.path.join(DEVROOT, 'orthanc', 'lua', 'wad_onstablestudy.lua'), 
                  inlist=inlist, 
                  outlist=outlist) 
    copy_replaces(src=os.path.join('templates', 'wadselector.py'), 
                  dest=os.path.join(DEVROOT, 'orthanc', 'lua', 'wadselector.py'), 
                  inlist=inlist, 
                  outlist=outlist) 

def build_static(database, branch=ORTHANCBRANCH, branchpg=POSTGRESQLBRANCH, branchwv=WEBVIEWERBRANCH):
    # build static, stand alone orthanc from latest sources, with relevant plugins
    #   apt-get -y install python wget nano build-essential unzip cmake mercurial uuid-dev
    opsys = platform.system() # Linux, Darwin, Windows
    if opsys == 'Windows':
        print('Building Orthanc and plugins on Windows is currently unsupported.\n'+
              'Please directly download from Orthanc website')
        return


    print("...building orthanc and selected plugins...")
    # build orthanc:
    result = subprocess.call(['bash', 'build.sh', branch], 
                             cwd=os.path.join(DEVROOT, 'orthanc', 'sources'))
    if not result == 0:
        print("Building orthanc was unsuccessful. Are all requirements installed?\n"+
              "On Ubuntu:\n  sudo apt-get -y install wget nano build-essential unzip cmake mercurial uuid-dev")
        sys.exit()

    # build orthanc-webviewer:
    result = subprocess.call(['bash', 'build-webviewer.sh', branchwv], 
                             cwd=os.path.join(DEVROOT, 'orthanc', 'sources'))
    if not result == 0:
        print("Building orthanc webviewer was unsuccessful. Are all requirements installed?\n"+
              "On Ubuntu:\n  sudo apt-get -y install wget nano build-essential unzip cmake mercurial uuid-dev")
        sys.exit()

    # build orthanc-postgresql:
    if database == 'postgresql':
        fname = 'build-postgresql.sh'
        if opsys == 'Darwin':
            print('Building postgresql backend for Orthanc on MacOSX is currently unsupported.\n'+
                  'Please directly download from Orthanc website')
            return

        result = subprocess.call(['bash', fname, branchpg], 
                                 cwd=os.path.join(DEVROOT, 'orthanc', 'sources'))
        if not result == 0:
            print("Building orthanc postgresql backed was unsuccessful. Are all requirements installed?\n"+
                  "On Ubuntu:\n  sudo apt-get -y install wget nano build-essential unzip cmake mercurial uuid-dev postgresql-server-dev-all")
            sys.exit()

def cleanup():
    # clean up 
    shutil.rmtree(os.path.join(DEVROOT, 'orthanc', 'sources'), ignore_errors=True)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Orthanc Setup for WAD_QC')
    mode = None
    database = None
    patch = None
    patches = [None,'fix360','fix362threads', 'fix164inf', 'fixpluginlogging']
    branches = [ORTHANCBRANCH_1_2_0, ORTHANCBRANCH]
    branch = ORTHANCBRANCH
    branchespg = [POSTGRESQLBRANCH_2_1, POSTGRESQLBRANCH]
    branchpg = POSTGRESQLBRANCH
    brancheswv = [WEBVIEWERBRANCH]
    branchwv = WEBVIEWERBRANCH

    parser.add_argument('-m','--mode',
                        default=mode,type=str,
                        help='mode of operation: either "update" or "build", where "update"'+
                        ' just copies the latest scripts into to correct location, and "build"'+
                        ' redownloads sources and builds orthanc from scratch',dest='mode')

    parser.add_argument('-d','--database',
                        default=database,type=str,
                        help='the database back engine of orthanc: either "sqlite" or "postgresql"',
                        dest='database')

    parser.add_argument('-p','--patch',
                        default=patch,type=str,
                        help='apply patch (one of None, {}) [{}]'.format(', '.join(patches[1:]), patch),
                        dest='patch')

    parser.add_argument('-b','--branch',
                        default=branch,type=str,
                        help='use branch (one of {}) [{}]'.format(', '.join(branches), branch),
                        dest='branch')

    parser.add_argument('-bp','--branch-pg',
                        default=branchpg,type=str,
                        help='use postgresql branch (one of {}) [{}]'.format(', '.join(branchespg), branchpg),
                        dest='branchpg')

    parser.add_argument('-bw','--branch-wv',
                        default=branchwv,type=str,
                        help='use webviewer branch (one of {}) [{}]'.format(', '.join(brancheswv), branchwv),
                        dest='branchwv')

    args = parser.parse_args()
    if not args.mode in ['build', 'update']:
        parser.print_help()
        exit()
    if not args.database in ['sqlite', 'postgresql']:
        parser.print_help()
        exit()
    if not args.patch is None:
        dopatches = args.patch.split(',')
    else:
        dopatches = []
       
    for patch in dopatches:
        if not patch in patches:
            parser.print_help()
            exit()

    """
    1. change to correct folder
    2. clean up left overs of previous build
    3. create folder structure
    4. create scripts from templates
    5. build orthanc and plugins
    6. clean up
    """
    os.chdir(os.path.dirname(os.path.abspath(__file__)))# this file's folder

    if args.mode == 'build':
        delete_old()

    create_folders()

    create_scripts(args.database, args.branchpg, dopatches)

    if args.mode == 'build':
        build_static(args.database, args.branch, args.branchpg, args.branchwv)

    cleanup()


