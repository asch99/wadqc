from flask import Blueprint, render_template, Markup, url_for, redirect, request, flash, send_file, session
try:
    from app.mod_auth.controllers import login_required
    from app.libs.shared import dbio_connect, bytes_as_string, string_as_bytes
except ImportError:
    from wad_dashboard.app.mod_auth.controllers import login_required
    from wad_dashboard.app.libs.shared import dbio_connect, bytes_as_string, string_as_bytes
dbio = dbio_connect()

import os
import tempfile
from io import BytesIO
from peewee import IntegrityError
import json, jsmin

from .forms_editor import EditorForm

# logging
from werkzeug.local import LocalProxy
from flask import current_app
logger = LocalProxy(lambda: current_app.logger)

mod_blueprint = Blueprint('editor', __name__, url_prefix='/waddashboard')

class WDException(Exception):
    def __init__(self,*args,**kwargs):
        Exception.__init__(self,*args,**kwargs)

@mod_blueprint.route('/editor/', methods=['GET', 'POST'])
@login_required
def default():
    # display and allow editing of json

    # not for normal users
    role = 2
    if session.get('logged_in'):
        role = session.get('role')
    if role >1: 
        return(redirect(url_for('waddashboard.home')))

    _mid = int(request.args['mid']) if 'mid' in request.args else None # id of meta

    # invalid request are to be ignored
    if _mid is None:
        logger.error("Must supply a meta id")
        # go back to overview page
        return redirect(url_for('waddashboard.home')) # catch wild call

    title = None
    dajson = None
    fname = None
    opt = {}

    try:
        meta = dbio.DBMetaConfigs.get_by_id(_mid)
    except dbio.DBMetaConfigs.DoesNotExist:
        logger.error("Must supply a valid meta id")
        # go back to overview page
        return redirect(url_for('waddashboard.home')) # catch wild call
    if meta:
        dajson = meta.val
        fname = 'meta'
        title = 'Edit meta'
        opt = {'mid': _mid}
        if len(meta.module_configs) > 0:
            fname = meta.module_configs[0].name
            title = "Edit meta: \"{}\"".format(fname)
    else:
        logger.error("Must supply a valid meta id")
        # go back to overview page
        return redirect(url_for('waddashboard.home')) # catch wild call
    
    dajson = json.dumps(json.loads(jsmin.jsmin(bytes_as_string(dajson))))
    subtitle = ''#dajson
        
    page = ''
    
    form = EditorForm(None if request.method=="GET" else request.form)

    return render_template("waddashboard/jsonedit.html", title=title, subtitle='', msg='', html=Markup(page),
                           inpanel={'type': "panel-info", 'title': "info", 'content':subtitle},
                           form=form,
                           json=Markup(dajson), fname=fname, witheditor=True, **opt)

@login_required
@mod_blueprint.route('/editor/download/', methods=['GET', 'POST'])
def download_from_editor():
    # download the json as shown in editor
    title = "ERROR"
    subtitle = ""
    page = ""

    # not for normal users
    role = 2
    if session.get('logged_in'):
        role = session.get('role')
    if role >1: 
        return(redirect(url_for('waddashboard.home')))

    try:
        data = json.loads(request.form.get('downloadform-posttable', None))
        if data is None:
            msg = "No data supplied"
            subtitle = msg
            raise WDException(msg)

        _dajson = data.get('json', None)
        _fname = data.get('fname', None)

        if not _dajson:
            msg = "Must supply a json"
            subtitle = msg
            raise WDException(msg)

        if not _fname:
            msg = "Must supply a fname"
            subtitle = msg
            raise WDException(msg)

        # make a bit more pretty
        blob = json.loads(_dajson)
        blob = string_as_bytes(json.dumps(blob, indent=4))
    
        return send_file(BytesIO(blob), as_attachment=True, attachment_filename="{}.json".format(_fname))#mimetype=None

    except WDException as e:
        logger.error(str(e))

    if title =="ERROR":
        inpanel = {'type': "panel-danger", 'title': "ERROR", 'content':subtitle}
    else:
        inpanel = {'type': "panel-success", 'title': "Succes", 'content':subtitle}
        
    return render_template("waddashboard/generic.html", title=title, subtitle='', msg='', html=Markup(page),
                           inpanel=inpanel)


@login_required
@mod_blueprint.route('/editor/store/', methods=['GET', 'POST'])
def store_from_editor():
    # store the json as shown in editor as the indicated config
    title = "ERROR"
    subtitle = ""
    page = ""

    # not for normal users
    role = 2
    if session.get('logged_in'):
        role = session.get('role')
    if role >1: 
        return(redirect(url_for('waddashboard.home')))

    try:
        data = json.loads(request.form.get('sendform-posttable', None))
        if data is None:
            msg = "No data supplied"
            subtitle = msg
            raise WDException(msg)
            
        _dajson = data.get('json', None)
        _mid = int(data['mid']) if 'mid' in data else None # id of meta

        # invalid request are to be ignored
        if _mid is None:
            msg = "Must supply a meta id"
            subtitle = msg
            raise WDException(msg)
    
        if not _dajson:
            msg = "Must supply a json"
            subtitle = msg
            raise WDException(msg)
    
        # store config
        try:
            meta = dbio.DBMetaConfigs.get_by_id(_mid)
        except dbio.DBMetaConfigs.DoesNotExist:
            msg = '"{}" is not a valid meta id!'.format(_mid)
            subtitle = msg
            raise WDException(msg)

        if meta:
            # validate meta
            valid, msg = validate_json("metafile", _dajson)
            if valid:
                blob = json.loads(jsmin.jsmin(_dajson))
            else:
                msg = 'Refusing to store invalid json as meta! {}'.format(msg)
                subtitle = msg
                raise WDException(msg)

            # save config
            meta.val = json.dumps(blob)
            meta.save()

            title = 'Success'
            subtitle = 'Stored modified meta '
            if len(meta.module_configs) > 0:
                fname = meta.module_configs[0].name
                subtitle += '"{} "'.format(fname)
            subtitle += 'in database.'
        else:
            msg = '"{}" is not a valid meta id!'.format(_mid)
            subtitle = msg
            raise WDException(msg)
    
    except WDException as e:
        logger.error(str(e))

    if title =="ERROR":
        inpanel = {'type': "panel-danger", 'title': "ERROR", 'content':subtitle}
    else:
        inpanel = {'type': "panel-success", 'title': "Succes", 'content':subtitle}
            
    return render_template("waddashboard/generic.html", title=title, subtitle='', msg='', html=Markup(page),
                           inpanel=inpanel)

# validate json
def validate_json(filetype, info):
    """
    validate if the uploaded json has the correct fields
    """
    valid = True
    msg = ""
    if info is None:
        return valid, msg

    try:
        blob = json.loads(jsmin.jsmin(info))
    except ValueError as e:
        valid = False
        msg = "file cannot be decoded as json: {}".format(e)
        return valid, msg

    if filetype == 'metafile':
        """
        results:
          <name>:
            <items>

        comments:
          <items>
        """
        # check for required items
        required = ['results', 'metaformat']
        for req in required:
            val = blob.get(req, None)
            if val is None:
                valid = False
                msg = "'{}' must be defined in meta.json".format(req)
                return valid, msg
            if req == 'metaformat':
                if not isinstance(val, str):
                    valid = False
                    msg = "Key '{}' in meta.json must be a string".format(key)
                    return valid, msg
            elif not isinstance(val, dict):
                valid = False
                msg = "Key '{}' in meta.json must be a dictionary".format(req)
                return valid, msg
        
        # check for extra items
        accepted = required
        required.extend(['comments'])
        for key,val in blob.items():
            if key not in accepted:
                valid = False
                msg = "Key '{}' invalid for meta.json".format(key)
                return valid, msg
                
    else:
        valid = False
        msg = "filetype '{}' is not a recognized json file to validate".format(filetype)
        logger.error(msg)

    return valid, msg