#!/usr/bin/env python
"""
stand alone test for combination of selector with db and running pacs

Changelog:
  20160426: implement multi-value for rule
  20160425: prefer dbio and pacsio init by dict
"""

__version__ = '20160426'

import sys, os

basedir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(basedir)
sys.path.append(os.path.join(basedir, 'wad_qc', 'connection'))

from wad_qc.connection.dbio import DBIO
from wad_qc.connection.pacsio import PACSIO
from wad_qc.connection import get_dbio_config, get_pacs_config

import subprocess

# Test a selector connected to local pacs and local db

# 1. make sure a local pacs is running and that it has the required demo data
# 2. populate a localdb with some selectors, and selectorrules
# 3. get all demo studies in pacs
# 4. call the selector with the proper demo study ids

rootfolder = os.path.dirname(os.path.abspath(__file__))
inifile = inifile = os.path.join(rootfolder, 'wadconfig.ini')

dbio_config = get_dbio_config(inifile)
pacs_config = get_pacs_config(inifile)

## 1. make sure there is a basic iqc db available
dbio = DBIO(dbio_config)

## 1. make sure a local pacs is running and that it has the required demo data
pacsio = PACSIO(pacs_config)
print('OK. Orthanc server running at %s...'%pacsio.URL)

# upload the demo dicom folder in the parent folder
logs = pacsio.uploadDicom(os.path.join(rootfolder,'Some_dicom_data'))
demostudies = []
for log in logs:
    studyid = pacsio.getStudyId(instanceid=log['ID'])
    if 'alreadystored' in log['Status'].lower():
        print('...ignoring: test dicom already in pacs')
        
    if not studyid in demostudies: demostudies.append(studyid)
print('(Re)added %d demo studies to PACS'%len(demostudies))

# 2. populate a localdb with some selectors, and selectorrules
# insert modules
moddicts = [
    {'name':'testmodule',
     'filepath':os.path.abspath(os.path.join(rootfolder,'Modules','TestModule','testmodule.py')),
     'description':'test module'}
    ]
for m in moddicts: 
    try:
        print('deleting module %s from db if exists'%m['name'])
        dbio.deletemodule(name=m['name'])
    except:
        print(' ...ignoring: module was not present in db')
    try:
        print('adding module %s to db'%m['name'])
        dbio.addmodule(m)
    except Exception as e:
        if 'unique constraint failed' in e.message.lower():
            print(' ...ignoring: test module already in iqcdb')
        else:
            raise
        
print('(Re)added %d demo modules to IQCDB'%len(moddicts))

# insert configs
fname = os.path.join(rootfolder,'Configs','testmodule_some_config.json')
with open(fname,'r') as f: data = f.read()
confdicts = [
    {'modulename':moddicts[0]['name'],
     'name':'config_test',
     'description':'test config',
     'val':data}
    ]
for c in confdicts:
    try:
        dbio.addconfig(c)
    except Exception as e:
        if 'unique constraint failed' in e.message.lower():
            print(' ...ignoring: test config already in iqcdb')
        else:
            raise
print('(Re)added %d demo configs to IQCDB'%len(confdicts))

# insert selectors
seldicts = [
    {'configname':confdicts[0]['name'],
     'configversion': 1,
     'name':'selector_test', 
     'description':'test selector',
     'datatypename':'dcm_series',
     'hidden':False,
     'frequency':12}
    ]
for s in seldicts:
    try:
        dbio.addselector(s)
    except Exception as e:
        if 'unique constraint failed' in e.message.lower():
            print(' ...ignoring: test selector already in iqcdb')
        else:
            raise
print('(Re)added %d demo selector to IQCDB'%len(seldicts))

# insert rules
ruledicts = [
    {'selectorname':seldicts[0]['name'],
     'dicomtag':'0008,0060', # Modality
     'logicname': 'equals',
     'values':['NM']},
    {'selectorname':seldicts[0]['name'],
     'dicomtag':'0008,103e', # SeriesDescription
     'logicname': 'contains',
     'values':['static']},
    {'selectorname':seldicts[0]['name'],
     'dicomtag':'0020,0013', # InstanceNumber
     'logicname': 'ends with',
     'values':['1']},
    ]
ruleids = []
for r in ruledicts:
    try:
        ruleids.append(dbio.addrule(r))
    except Exception as e:
        if 'unique constraint failed' in e.message.lower():
            print(' ...ignoring: test rule already in iqcdb')
        else:
            raise
print('(Re)added %d demo rules to IQCDB'%len(ruledicts))

# 3. get all demo studies in pacs and call the selector with the proper demo study ids
nproc = len(dbio.getProcesses2())
for studyid in demostudies: 
    subprocess.call(['python3',os.path.join(os.path.dirname(rootfolder),'selector.py'),'-s',studyid,'-i',inifile])
nproc = len(dbio.getProcesses2())-nproc
print('Finally added %d new (possibly non-unique) processes to IQCDB'%nproc)

# 4. cleanup demo stuff
if 1<0:
    # clean up: remove demo dicom from PACS
    for studyid in demostudies: 
        pacsio.deleteStudy(studyid)
    # clean up: remove demo rules from IQCDB
    for r in ruleids:
        dbio.deleterule(pk=r)
    # clean up: remove demo selectors from IQCDB
    for s in seldicts:
        dbio.deleteselector(name=s['name'])
    # clean up: remove demo configs from IQCDB
    for c in confdicts:
        dbio.deleteconfig(name=c['name'],versionnumber=c['versionnumber'])
    # clean up: remove demo modules from IQCDB
    for m in moddicts:
        dbio.deletemodule(name=m['name'])



