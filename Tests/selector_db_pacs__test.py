#!/usr/bin/env python
from __future__ import print_function
"""
stand alone test for combination of selector with db and running pacs

Changelog:
  20160610: updated to match changes in dbio
  20160606: split inifile in dbconfig and setup
  20160530: new dbio
  20160523: go peewee
  20160513: python3 compatible; added logger
  20160426: implement multi-value for rule
  20160425: prefer dbio and pacsio init by dict
"""

__version__ = '20160610'

import logging
logging.basicConfig(format='[%(levelname)s:%(module)s:%(funcName)s]: %(message)s',level=logging.INFO)
#logging.basicConfig(format='%(asctime)s [%(levelname)s:%(module)s:%(funcName)s]: %(message)s',level=logging.DEBUG)

import os
try: 
    # this will fail unless wad_qc is already installed
    from wad_qc.connection import dbio
    DEVMODE=False
    logging.info("** using installed package wad_qc **")
except ImportError: 
    import sys
    # add parent folder to search path for modules
    sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    from wad_qc.connection import dbio
    DEVMODE=True
    logging.info("** development mode **")

from wad_qc.connection.pacsio import PACSIO
import tempfile
import shutil
import subprocess
import test_base

rootfolder = os.path.dirname(os.path.abspath(__file__)) # here this folder

moddicts = test_base.multidict(test_base.moddict, 1, ignore=['uploadfilepath', 'filename', 'origin'])

confdicts = test_base.multidict(test_base.confdict, 1, ignore=['modulename', 'origin', 'datatypename'])
for c,m in zip(confdicts, moddicts):
    c['modulename'] = m['name']

seldicts = test_base.multidict(test_base.seldict, 1, ignore=['configname', 'datatypename'])
for c,s in zip(confdicts, seldicts):
    s['configname'] = c['name']
    s['hidden'] = False

ruledicts = [
    {'selectorname':seldicts[0]['name'],
     'dicomtag':'0008,0060', # Modality
     'logicname': 'equals',
     'values':['NM']},
    {'selectorname':seldicts[0]['name'],
     'dicomtag':'0008,103e', # SeriesDescription
     'logicname': 'contains',
     'values':['static']},
    {'selectorname':seldicts[0]['name'],
     'dicomtag':'0020,0013', # InstanceNumber
     'logicname': 'ends with',
     'values':['1']},
    ]

def upload_dicom(pacsio):
    logs = pacsio.uploadDicomFolder(os.path.join(rootfolder,'Some_dicom_data'))
    demostudies = []
    for log in logs:
        studyid = pacsio.getStudyId(instanceid=log['ID'])
        if 'alreadystored' in log['Status'].lower():
            logging.info('...ignoring: test dicom already in pacs')
            
        if not studyid in demostudies: demostudies.append(studyid)
    logging.info('(Re)added %d demo studies to PACS'%len(demostudies))
    return demostudies

def add_modules():
    # insert modules
    for m in moddicts: 
        logging.info('adding module "%s" to db'%m['name'])
        module = dbio.DBModules.create(**m)
            
    logging.info('Added %d demo modules to IQCDB'%len(moddicts))

def add_configs():
    # insert configs
    fname = os.path.join(rootfolder,'Configs','testmodule_some_config.json')
    with open(fname,'r') as f: data = f.read()
    for c in confdicts:
        c['val'] = data
        config = dbio.DBModuleConfigs.create(**c)
    logging.info('Added %d demo configs to IQCDB'%len(confdicts))

def add_selectors():        
    # insert selectors
    for s in seldicts:
        dbio.DBSelectors.create(**s)
    logging.info('Added %d demo selector to IQCDB'%len(seldicts))

def add_rules():
    # insert rules
    for r in ruledicts:
        fielddict = r.copy()
        sel = dbio.DBSelectors.get(dbio.DBSelectors.name == r['selectorname'])
        del fielddict['selectorname']
        sel.addRule(**fielddict)
    logging.info('Added %d demo rules to IQCDB'%len(ruledicts))

def run_selector_on_studies(sourcename, demostudies):
    nproc = len(dbio.DBProcesses.select())
    for studyid in demostudies:
        if DEVMODE:
            subprocess.call(['python',os.path.join(os.path.dirname(rootfolder),'wad_core','selector.py'),'-n',sourcename,'-s',studyid,'-i',inifile])
        else:
            subprocess.call(['wadselector','-n',sourcename,'-s',studyid,'-i',inifile])
    nproc = len(dbio.DBProcesses.select())-nproc
    logging.info('Finally added %d new (possibly non-unique) processes to IQCDB'%nproc)

def clean_up_pacs(pacsio, demostudies):
    for studyid in demostudies: 
        pacsio.deleteStudy(studyid)
    
def clean_up_db():
    # clean up: remove demo rules from IQCDB
    for r in ruleids:
        dbio.deleterule(pk=r)
    # clean up: remove demo selectors from IQCDB
    for s in seldicts:
        dbio.deleteselector(name=s['name'])
    # clean up: remove demo configs from IQCDB
    for c in confdicts:
        dbio.deleteconfig(name=c['name'])
    # clean up: remove demo modules from IQCDB
    for m in moddicts:
        dbio.deletemodule(name=m['name'])

if __name__ == "__main__":
    # Test a selector connected to local pacs and local db

    # 1. make sure a local pacs is running and that it has the required demo data
    # 2. populate a localdb with some selectors, and selectorrules
    # 3. get all demo studies in pacs
    # 4. call the selector with the proper demo study ids
    leave_clean = False # should we remove all testing stuff from PACS and from DB after testing?

    inifile = os.path.join(rootfolder,'wadconfig.ini')
    setupfile = os.path.join(rootfolder,'wadsetup.ini') # for testing, use the test config
    logging.info('')

    ## 1. make sure there is a basic iqc db available
    test_base.dbfreshstart(dbio, inifile, setupfile)

    pacsconfig = dbio.DBDataSources.select()[0].as_dict()
    ## 1. make sure a local pacs is running and that it has the required demo data
    pacsio = PACSIO(pacsconfig) # for testing, use the test config
    logging.info('OK. Orthanc server running at %s...'%pacsio.URL)

    # upload the demo dicom folder in the parent folder
    demostudies = upload_dicom(pacsio)
    
    # 2. populate a localdb with some selectors, and selectorrules
    add_modules()
    add_configs()
    add_selectors()
    add_rules()
    
    # 3. get all demo studies in pacs and call the selector with the proper demo study ids
    run_selector_on_studies(pacsconfig['name'], demostudies)

    # 4. cleanup demo stuff
    if leave_clean: clean_up_db()

    # clean up: remove demo dicom from PACS
    if leave_clean: clean_up_pacs(pacsio, demostudies)
    


