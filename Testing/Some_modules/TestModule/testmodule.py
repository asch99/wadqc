#!/usr/bin/env python3
import os

from wad_qc.module import pyWADinput

# now defaults to json files as input
# ./testmodule.py -d ../../Some_dicom_data/Some_study -c ../../Configs/testmodule_some_config.json -r results.json

def testFunction(data,results,action):
    try:
        level = action['default_level']
    except KeyError:
        level = None

    for instance in data.getAllInstances():
        results.addString('SeriesDescription', instance.SeriesDescription)
        print(action['filters'])
    
    results.addBool('Some bool', True, val_equal=True)
    results.addFloat('Some float', 1.234, val_min=0.5, val_max=1.5)
    
    with open('some_object.txt', 'w') as f:
        f.write("Rather boring object")    
    results.addObject('Some object', 'some_object.txt')
    
    print("Here's something for stdout!")
        
    #raise ValueError("And here for sterr!")
    
        
if __name__ == "__main__":
    data, results, config = pyWADinput()

    print("First!")
    # read runtime parameters for module
    for name,action in config['actions'].items():
        # here use action['filters'] to select the proper dataset if any
        if name == 'myFirstTest':
            testFunction(data, results, action)
    print("Some more text")
    import time
    import random
    time.sleep(0.5*random.random())
    #time.sleep(5)

    print("BARF!")
    results.addString('SomeResult', 'Well, something " happened')
    #raise ValueError("Boink!")
    results.write()
    
    time.sleep(5)
    
    print("Ater 5 sec of sleeping...")
