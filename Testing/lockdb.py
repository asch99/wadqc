import sqlite3
try:
    import configparser
except ImportError:
    import ConfigParser as configparser

inifile = 'wadconfig.ini'

config = configparser.SafeConfigParser()
with open(inifile,'r') as f:
    config.readfp(f)

dbname = config['iqc-db']['DBASE']

con = sqlite3.connect(dbname)
con.isolation_level = 'EXCLUSIVE'
con.execute('BEGIN EXCLUSIVE')
input('Press key to unlock')
con.close()
