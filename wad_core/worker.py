"""
This is a separate file, as for all spawned worker processes, the source file of the worker is reread by each worker!
https://pymotw.com/2/multiprocessing/basics.html
"""

from __future__ import print_function
import time
try:
    from queue import Empty, Full
except ImportError:
    from Queue import Empty, Full
from wad_qc.connection import dbio
from wad_core import analyser

from logging import getLogger
LOGGERNAMEWORKERS = 'wad_control' # could define a separate logfile if this one gets too cluttered
logger = getLogger(LOGGERNAMEWORKERS)


def worker(nr, stop_flag, task_queue, inifile):
    """
    Not a separate class, just a function
    """
    logger.info("Worker-{} activated".format(nr))
    #dbio.db_connect(inifile) # each worker gets its own connection to the database
    while not stop_flag.value:
        try:
            processid = task_queue.get(False)
        except Empty:
            time.sleep(0.1)
        except Full:
            time.sleep(0.1)
        else:
            try:
                dbio.db_connect(inifile) # each worker gets its own connection to the database
            except Exception as e:
                logger.error("Worker-{} cannot connect to database: {}".format(nr, str(e)))
                task_queue.put(processid) # put it back in the queue
            else:
                logger.info("Worker-{} starting process {}".format(nr, processid))
                try:
                    analyser.run(dbio, processid)
                except Exception as e:
                    logger.info("Worker-{} error for process {}: {}".format(nr, processid, str(e)))
                dbio.db.close()
                logger.info("Worker-{} finished process {}".format(nr, processid))

    logger.info("Worker-{} shut down".format(nr))
